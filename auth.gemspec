$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "auth/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "auth_engine"
  s.version     = Auth::VERSION
  s.authors     = "Pavlo Shabat"
  s.email       = "pavlo.shabat@perfectial.com"
  #s.homepage    = "TODO"
  s.summary     = "This is authentication module for host application"
  s.description = "This is authentication module for host application"

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 3.2.11"
  s.add_dependency 'devise'
  # s.add_dependency "jquery-rails"

  s.add_development_dependency "pg"
end
