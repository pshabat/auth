Auth::Engine.routes.draw do
  get "home/index" =>'home#index'

  devise_for :users, :class_name => "Auth::User", module: :devise#, controllers: { sessions: "devise/sessions", registrations: "devise/registrations" }
  #devise_for :users, path: 'profile', controllers: { registrations: "registrations" }
end
